package com.mitocode.controller;

import java.io.IOException;
import java.io.Serializable;

import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import com.mitocode.model.Usuario;

@Named
@ViewScoped
public class MasterBean implements Serializable {
	
	private static final long serialVersionUID = 1L;

	public void verificarSession() throws IOException {
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			Usuario us = (Usuario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuario");
			
			if(us == null) {
				context.getExternalContext().redirect("./../index.xhtml");
			}
		}catch (Exception e) {
			context.getExternalContext().redirect("./../index.xhtml");
		}
	}
	
	public void cerrarSession() {
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
	}

}
