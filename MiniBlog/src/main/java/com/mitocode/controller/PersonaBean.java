package com.mitocode.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.FileUploadEvent;

import com.mitocode.model.Persona;
import com.mitocode.service.IPersonaService;

@Named
@ViewScoped
public class PersonaBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private IPersonaService service;
	private Persona persona;
	private List<Persona> lista;
	private String tipoDialogo;
	
	/*
	public PersonaBean() {
		
		//this.service = new PersonaServiceImpl();
	}*/
	
	@PostConstruct
	public void init() {
		this.persona = new Persona();
		this.listar();
		this.tipoDialogo = "Nuevo";
	}
	
	public void handleFileUpload(FileUploadEvent event) {
		try {
			if (event.getFile() != null) {
				this.persona.setFoto(event.getFile().getContents());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public void operar(String accion) {
		try {
			
			if(accion.equalsIgnoreCase("R")) {
				service.registrar(persona);
			}else if(accion.equalsIgnoreCase("M")) {
				service.modificar(persona);
			}
			
			this.listar();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void listar() {
		try {
			this.lista = service.listar();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void mostrarData(Persona p) {
		this.persona = p;
		this.tipoDialogo = "Modificar";
	}
	
	public void limpiarControles() {
		this.persona = new Persona();
		this.tipoDialogo = "Nuevo";
	}
	
	/**
	 * GETTERS Y SETTERS
	 * @return
	 */

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public List<Persona> getLista() {
		return lista;
	}

	public void setLista(List<Persona> lista) {
		this.lista = lista;
	}

	public String getTipoDialogo() {
		return tipoDialogo;
	}

	public void setTipoDialogo(String tipoDialogo) {
		this.tipoDialogo = tipoDialogo;
	}
	
}
