package com.mitocode.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.mindrot.jbcrypt.BCrypt;

import com.mitocode.model.Usuario;
import com.mitocode.service.IUsuarioService;
import com.mitocode.util.MensajeManager;

@Named
@ViewScoped
public class UsuarioBean implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Inject
	private IUsuarioService service;
	
	@Inject
	private MensajeManager mensajeManajer;
	
	private List<Usuario> usuarios;
	private Usuario usuario;
	
	private String nombreUsuario;
	private String tipoDialogo;
	
	private String password;
	private String nuevoPassword;
	
	private boolean activaBotonAceptar;
	private boolean desactivarBotonValidar;
	
	
	@PostConstruct
	public void init() {
		this.usuario = new Usuario();
		this.activaBotonAceptar = false;
		this.desactivarBotonValidar = false;
		this.listar();
	}
	
	public void listar() {
		try {
			this.usuarios = service.listar();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void bucarUsuario() {
		try {
			this.usuarios = service.buscarPorNombreUsuario(this.nombreUsuario);
			this.nombreUsuario = "";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void modificar(Usuario u) {
		String header = "Modificar Usuario: " + u.getUsuario();
		this.usuario = u;
		this.tipoDialogo = header;
	}
	
	public void validarPassword(String password) {
		if(BCrypt.checkpw(password, usuario.getContrasena())) {
			this.activaBotonAceptar = true;
			this.desactivarBotonValidar = true;
			this.mensajeManajer.mostrarMensaje("Aviso: El password es correcto.", "El password es correcto.", "INFO");
		}else {
			this.activaBotonAceptar = false;
			this.desactivarBotonValidar = false;
			this.mensajeManajer.mostrarMensaje("Aviso: El password es incorrecto.", "El password es incorrecto.", "ERROR");
		}
	}
	
	public void actualizaeClave() {
		String nuevaClave = this.nuevoPassword;
		String hasNuevaClave = BCrypt.hashpw(nuevaClave, BCrypt.gensalt());
		this.usuario.setContrasena(hasNuevaClave);
		
		System.out.println(nuevaClave);
		System.out.println(hasNuevaClave);
		
		try {
			Integer m = this.service.modificar(usuario);
			if(m > 0)	this.mensajeManajer.mostrarMensaje("Aviso: El password fue actualizado correctamente.", "El password fue actualizado correctamente.", "INFO");
			else	this.mensajeManajer.mostrarMensaje("Aviso: El password no fue actualizado. \n Intente nuevamente.", "El password no fue actualizado.", "ERROR");
			this.limpiarControles();
			
		} catch (Exception e) {
			this.mensajeManajer.mostrarMensaje("Aviso: Tu peticion no pudo ser atendida.", "Tu peticion no pudo ser atendida.", "ERROR");
			this.limpiarControles();
			e.printStackTrace();
		}
	}
	
	public void limpiarControles() {
		this.usuario = new Usuario();
		this.activaBotonAceptar = false;
		this.desactivarBotonValidar = false;
		this.password = "";
		this.nuevoPassword = "";
		this.nombreUsuario = "";
	}

	//GETTER AND SETTER
	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getTipoDialogo() {
		return tipoDialogo;
	}

	public void setTipoDialogo(String tipoDialogo) {
		this.tipoDialogo = tipoDialogo;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNuevoPassword() {
		return nuevoPassword;
	}

	public void setNuevoPassword(String nuevoPassword) {
		this.nuevoPassword = nuevoPassword;
	}

	public boolean isActivaBotonAceptar() {
		return activaBotonAceptar;
	}

	public void setActivaBotonAceptar(boolean activaBotonAceptar) {
		this.activaBotonAceptar = activaBotonAceptar;
	}

	public boolean isDesactivarBotonValidar() {
		return desactivarBotonValidar;
	}

	public void setDesactivarBotonValidar(boolean desactivarBotonValidar) {
		this.desactivarBotonValidar = desactivarBotonValidar;
	}

	public MensajeManager getMensajeManajer() {
		return mensajeManajer;
	}

	public void setMensajeManajer(MensajeManager mensajeManajer) {
		this.mensajeManajer = mensajeManajer;
	}
	
}
