package com.mitocode.controller;

import java.io.File;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.primefaces.model.chart.PieChartModel;

import com.mitocode.service.ISeguidorService;
import com.mitocode.util.ReporteSeguidor;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Named
@ViewScoped
public class ReporteBean implements Serializable {
	
	@Inject
	private ISeguidorService serguidorService;
	
	private List<ReporteSeguidor> lista;
	
	private PieChartModel pieModel1;
	
	@PostConstruct
	public void init() {
		this.listarSeguidores();
		this.crearPieModel();
	}
	
	public void generarReporte() {
		try {
			Map<String, Object> parametros = new HashMap<String, Object>();
			//parametros.put("", ""); 
			
			File jasper = new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/resources/reports/consultas.jasper"));
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasper.getPath(), parametros, new JRBeanCollectionDataSource(this.lista));
			
			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
			response.addHeader("Content-disposition", "attachment; filename=mini-blog.pdf");
			ServletOutputStream stream = response.getOutputStream();
			
			JasperExportManager.exportReportToPdfStream(jasperPrint, stream);
			
			stream.flush();
			stream.close();
			FacesContext.getCurrentInstance().responseComplete();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}
	
	public void verPDF() {
		try {

			File jasper = new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/resources/reports/consultas.jasper"));

			byte[] bytes = JasperRunManager.runReportToPdf(jasper.getPath(), null, new JRBeanCollectionDataSource(this.lista));
			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
			response.setContentType("application/pdf");
			response.setContentLength(bytes.length);

			ServletOutputStream outStream = response.getOutputStream();
			outStream.write(bytes, 0, bytes.length);
			outStream.flush();
			outStream.close();

			FacesContext.getCurrentInstance().responseComplete();

		} catch (Exception e) {

		}
	}
	
	public void crearPieModel() {
		this.pieModel1 = new PieChartModel();
		this.lista.forEach(x ->{
			pieModel1.set(x.getPublicador(), x.getCantidad());
		});
		
		this.pieModel1.setTitle("Cantidad de Seguidores");
		this.pieModel1.setLegendPosition("W");
		this.pieModel1.setShowDataLabels(true);
	}
	
	public void listarSeguidores() {
		try {
			this.lista = serguidorService.listarSeguidores();
		}catch (Exception e) {
			// TODO: handle exception
		}
	}

	public PieChartModel getPieModel1() {
		return pieModel1;
	}

	public void setPieModel1(PieChartModel pieModel1) {
		this.pieModel1 = pieModel1;
	}

	public List<ReporteSeguidor> getLista() {
		return lista;
	}

	public void setLista(List<ReporteSeguidor> lista) {
		this.lista = lista;
	}
	
	

}
