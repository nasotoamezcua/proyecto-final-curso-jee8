package com.mitocode.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.DualListModel;

import com.mitocode.model.Persona;
import com.mitocode.model.Rol;
import com.mitocode.model.Usuario;
import com.mitocode.service.IPersonaService;
import com.mitocode.service.IRolService;

@Named
@ViewScoped
public class AsignarBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private IPersonaService personaService;
	@Inject
	private IRolService rolService;
	
	private Persona persona;
	private List<Persona> personas;
	private DualListModel<Rol> dual;
	
	
	
	@PostConstruct
	public void init() {
		this.listarPersonas();
		this.listaRoles();
	}
	
	public void listarPersonas() {
		try {
			this.personas = personaService.listar(); 
		}catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public void listaRoles() {
		try {
			List<Rol> roles = this.rolService.listar();
			this.dual = new DualListModel<>(roles, new ArrayList<Rol>());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void asignar() {
		try {
			Usuario us = new Usuario();
			us.setIdUsuario(this.persona.getIdPersona());
			us.setPersona(this.persona);
			rolService.asignar(us, this.dual.getTarget());
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	

	/**
	 * 
	 * GETTERS Y SETTERS
	 * @return
	 */
	public List<Persona> getPersonas() {
		return personas;
	}

	public void setPersonas(List<Persona> personas) {
		this.personas = personas;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public DualListModel<Rol> getDual() {
		return dual;
	}

	public void setDual(DualListModel<Rol> dual) {
		this.dual = dual;
	}

}
