package com.mitocode.service.impl;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Named;

import org.mindrot.jbcrypt.BCrypt;

import com.mitocode.dao.IUsuarioDAO;
import com.mitocode.model.Usuario;
import com.mitocode.service.IUsuarioService;

@Named
public class UsuarioServiceImpl implements IUsuarioService, Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@EJB
	private IUsuarioDAO dao;
	

	@Override
	public Integer registrar(Usuario t) throws Exception {
		return dao.registrar(t);
	}

	@Override
	public Integer modificar(Usuario t) throws Exception {
		return dao.modificar(t);
	}

	@Override
	public Integer eliminar(Usuario t) throws Exception {
		return dao.eliminar(t);
	}

	@Override
	public List<Usuario> listar() throws Exception {
		return dao.listar();
	}

	@Override
	public Usuario listarPorId(Usuario t) throws Exception {
		return dao.listarPorId(t);
	}
	
	@Override
	public Usuario login(Usuario us) {
		String clave = us.getContrasena();
		String claveHash = dao.traerPassHahed(us.getUsuario());
		
		if(BCrypt.checkpw(clave, claveHash)) {
			return dao.leerPorNombreUsuario(us.getUsuario());
		}
		
		return new Usuario();
	}

	@Override
	public Usuario leerPorNombreUsuario(String us) {
		return dao.leerPorNombreUsuario(us);
	}

	@Override
	public List<Usuario> buscarPorNombreUsuario(String us) throws Exception {
		return dao.buscarPorNombreUsuario(us);
	}

}
