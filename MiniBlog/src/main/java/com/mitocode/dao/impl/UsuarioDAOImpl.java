package com.mitocode.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.mitocode.dao.IUsuarioDAO;
import com.mitocode.model.Usuario;

@Stateless
public class UsuarioDAOImpl implements IUsuarioDAO{
	
	@PersistenceContext(unitName = "blogPU")
	private EntityManager em;

	@Override
	public Integer registrar(Usuario t) throws Exception {
		em.persist(t);
		return t.getIdUsuario();
	}

	@Override
	public Integer modificar(Usuario t) throws Exception {
		em.merge(t);
		return t.getIdUsuario();
	}

	@Override
	public Integer eliminar(Usuario t) throws Exception {
		em.remove(em.merge(t));
		return 1;
	}

	@Override
	public List<Usuario> listar() throws Exception {
		
		List<Usuario> lista = new ArrayList<>();
		
		try {
			Query q = em.createQuery("SELECT us FROM Usuario us");
			lista = (List<Usuario>)q.getResultList();
		}catch (Exception e) {
			throw e;
		}
		
		return lista;
	}

	@Override
	public Usuario listarPorId(Usuario t) throws Exception {
		Usuario us = new Usuario();
		List<Usuario> lista = new ArrayList<>();
		
		try {
			Query q = em.createQuery("FROM Usuario us WHERE us.idUsuario = ?1");
			q.setParameter(1, t.getPersona().getIdPersona());
			
			lista = (List<Usuario>)q.getResultList();
			
			if(lista != null && !lista.isEmpty()) {
				us = lista.get(0);
			}
		}catch (Exception e) {
			throw e;
		}
		
		return us;
	}

	@Override
	public String traerPassHahed(String us) {
		
		Usuario usuario = new Usuario();
		
		try {
			Query q = em.createQuery("FROM Usuario u WHERE u.usuario = ?1");
			q.setParameter(1, us);
			
			List<Usuario> lista = (List<Usuario>)q.getResultList();
			if(!lista.isEmpty()) {
				usuario = lista.get(0);
			}
			
		}catch (Exception e) {
			throw e;
		}
		return usuario != null && usuario.getIdUsuario() != null ? usuario.getContrasena() : "nopass";
	}

	@Override
	public Usuario leerPorNombreUsuario(String us) {
		Usuario usuario = new Usuario();
		List<Usuario> lista = new ArrayList<>();
		
		try {
			Query q = em.createQuery("FROM Usuario us WHERE us.usuario = ?1");
			q.setParameter(1, us);
			
			lista = (List<Usuario>)q.getResultList();
			
			if(lista != null && !lista.isEmpty()) {
				usuario = lista.get(0);
			}
		}catch (Exception e) {
			throw e;
		}
		return usuario;
	}

	@Override
	public List<Usuario> buscarPorNombreUsuario(String us) {
		String usParametro = new StringBuilder().append("%").append(us).append("%").toString();
		List<Usuario> lista = new ArrayList<>();
		
		try {
			Query q = em.createQuery("FROM Usuario us WHERE LOWER(us.usuario) LIKE ?1");
			q.setParameter(1, usParametro.toLowerCase());
			
			lista = (List<Usuario>)q.getResultList();
			
		}catch (Exception e) {
			throw e;
		}
		
		return lista;
	}

}
